/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sort.strategy;

import sort.methods.IStrategy;

/**
 *
 * @author Zosia
 */
public class StrategyContext {
    private IStrategy strategy;
    
    public StrategyContext(IStrategy strategy){
        this.strategy = strategy;
    }
    
    public void perform(double[] array){
        strategy.Sort(array);
    }
}
