/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mystrategy;

import data.DataGenerator;
import sort.methods.Bubblesort;
import sort.methods.Insertionsort;
import sort.methods.Quicksort;
import sort.methods.Selectionsort;
import sort.strategy.StrategyContext;

/**
 *
 * @author LeopardProMK
 */
public class Client {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        double[] dataNonSort = DataGenerator.generate(100000);
        
        StrategyContext bubble = new StrategyContext(new Bubblesort());
        long bubblestart = System.currentTimeMillis();
	bubble.perform(dataNonSort);
	long bubblestop = System.currentTimeMillis();
	System.out.println("Bubblesort: " + (bubblestop - bubblestart) + " ms");
        
        StrategyContext insertion = new StrategyContext(new Insertionsort());
        long insertstart = System.currentTimeMillis();
	insertion.perform(dataNonSort);
	long insertstop = System.currentTimeMillis();
	System.out.println("Insertionsort: " + (insertstop - insertstart) + " ms");
        
        StrategyContext quicksort = new StrategyContext(new Quicksort());
        long quickstart = System.currentTimeMillis();
	quicksort.perform(dataNonSort);
	long quickstop = System.currentTimeMillis();
	System.out.println("Quicksort: " + (quickstop - quickstart) + " ms");
        
        StrategyContext selection = new StrategyContext(new Selectionsort());
        long selectionstart = System.currentTimeMillis();
	selection.perform(dataNonSort);
	long selectionstop = System.currentTimeMillis();
	System.out.println("Selectionsort: " + (selectionstop - selectionstart) + " ms");
    }
}

/*
    ODPOWIEDZI NA PYTANIA:
    1. Proszę o podanie głównego celu zastosowania wzorca strategii.
        Wzorzec strategii definiuje wiele algorytmów w postaci klas i umożliwia wymienne 
    stosowanie każdego z nich w trakcie działania programu. A więc głównym celem jego 
    zastosowania jest umożliwienie wymiennego korzystania z zaimplementowanych algorytmów
    w zależności od potrzeb.

    2. Czy istnieje możliwość zmiany algorytmu w czasie działania programu?
        Tak, jest to główna zaleta wykorystania tego wzorca. Jest to możliwe dzięki
    klasie StrategyContext.

    3. W jaki sposób wzorzec enkapsuluje poszczególne algorytmy?
        Implementacja interface'u IStrategy w każdym z algorytmów sortowania umożliwia 
    wykorzystanie metod każdej z klas za pomocą publicznych metod.

    4. Kiedy stosować wzorzec strategii?
        Wzorzec strategii należy stosować w przypadku, gdy w danej aplikacji istnieje wiele możliwości, 
    wyborów, np.: jak w tym projekcie można wybrać jeden z czterech algorytmow sortowania.
    Dzięki jego zastosowaniu można umożliwić wybór danego algorytmu podczas działania programu.
*/